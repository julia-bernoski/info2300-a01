# INFO2300-A01
This is a note taking web application. The purpose of this app is to be able to create multiple different notes and save to your local storage. 
The application consists of one page that can be saved and cleared. When you begin typing a note the application begins to save the data being entered into your Local Repository. It stores both the title of the note and the body of the note. When you click on "New Note", the page will be cleared and you can begin creating a new note.
Although the note was cleared you can go back and view the previous note created, this will be found within the side menu. This application also allows you to change your text by making it bold, a bigger font, underlining, as well as be able to change the alignment of the text. 
## Installation

Clone the Repository:
```bash
git clone https://julia-bernoski@bitbucket.org/julia-bernoski/info230-0a01.git
```
Create your own repository on the command line:
```bash
- git init
- git add README.md
- git commit -m "first commit"
- git add orgin https://julia-bernoski@bitbucket.org/julia-bernoski/info230-0a01.git
- git push -u orgin master
```
or you can simply download the repository and view it that way. 
## License
[View License Here](https://bitbucket.org/julia-bernoski/info2300-a01/src/master/LICENSE.md)

The License I went with was MIT as it is the most permissive software licenses out there. The MIT license is a great choice because it allows me to share my code under a copyleft license without forcing others to expose their proprietary code, it �s also business friendly which is great for Capstone projects and is also open source friendly while still allowing for monetization. 